import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By


class TestSelectSelenium(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.close()

    def test_single_checkbox_demo(self):
        driver = self.driver
        driver.get("https://www.seleniumeasy.com/test/basic-checkbox-demo.html")

        time.sleep(2)
        options_list = [
            "//label[text()='Click on this check box']"
        ]
        for s in options_list:
            check_button = driver.find_element_by_xpath(s)
            check_button.click()
        time.sleep(2)


if __name__ == '__main__':
    unittest.main()
