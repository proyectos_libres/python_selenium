import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By


class TestSelectSelenium(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.close()

    def test_sigle_input_field(self):
        driver = self.driver
        driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html")
        text_input_1 = driver.find_element_by_xpath("//input[@id='user-message']")
        show_button = driver.find_element_by_xpath("//button[contains(text(),'Show Message')]")
        text_output = driver.find_element_by_xpath('//*[@id="display"]')

        text_input_1.send_keys('Darwin Durand')
        time.sleep(5)
        show_button.click()
        self.assertEqual('Darwin Durand', text_output.text)
        time.sleep(2)


if __name__ == '__main__':
    unittest.main()
