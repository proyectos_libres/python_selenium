import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By


class TestSelectSelenium(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.close()

    def test_radio_button_demo(self):
        driver = self.driver
        driver.get("https://www.seleniumeasy.com/test/basic-radiobutton-demo.html")

        radio_button_male = driver.find_element_by_xpath(
            "//body/div[@id='easycont']/div[1]/div[2]/div[2]/div[2]/div[1]/label[1]/input[1]"
        )
        get_values_button = driver.find_element_by_xpath(
            "//button[contains(text(),'Get values')]"
        )
        text_output = driver.find_element_by_xpath(
            "//body/div[@id='easycont']/div[1]/div[2]/div[2]/div[2]/p[2]"
        )

        radio_button_male.click()
        time.sleep(2)
        get_values_button.click()
        time.sleep(2)
        self.assertRegex(text_output.text, "Male")
        time.sleep(2)


if __name__ == '__main__':
    unittest.main()